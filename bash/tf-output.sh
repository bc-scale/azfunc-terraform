#!/bin/bash
<< COMMENT
Test pipeline actions which process and store process tf outputs and store in Az App Config
COMMENT

TF_OUTPUT=$(terraform output -json)
#Build json of key:values from all tf outputs. Slurp results of for loop into one file for import later
for OUTPUT in $(echo $TF_OUTPUT| jq -r '.| keys[]')
do
    echo {\"$OUTPUT\":\"VALUE=$(echo $TF_OUTPUT| jq -r .$OUTPUT.value)\"}
done | jq --slurp 'reduce .[] as $item ({}; . * $item)'