/**
  * # Examples
  * ```
  * # Put below in versions.tf file
  * terraform {
  *   backend "azurerm" {}
  *   required_providers {
  *     azurerm = {
  *       source  = "hashicorp/azurerm"
  *       version = "~>2.65"
  *     }
  *   }
  *   required_version = ">= 1.0.0"
  * }
  * 
  * # Put below in provider.tf file
  * provider "azurerm" {
  *   features {}
  * }
  * 
  * # Put below in variables.tf file
  * variable "group_name" {
  *   type = string
  *   default = "azfunct-20210706"
  * }
  * 
  * variable "location" {
  *   type = string
  *   default = "uksouth"
  * }
  * 
  * variable "stage" {
  *   type = string
  *   default = "local"
  * }
  * 
  * # Put below in outputs.tf
  * output "primary_name" {
  *   value = module.primary.azfunc_name
  * }
  * 
  * output "secondary_name" {
  *   value = module.secondary.azfunc_name
  * }
  * 
  * # Start of main.tf
  * resource "random_id" "id" {
  *   byte_length = 4
  * }
  * 
  * resource "azurerm_resource_group" "this" {
  *   name     = var.group_name
  *   location = var.location
  * }
  * 
  * resource "azurerm_storage_account" "this" {
  *   name                      = "${var.stage}${random_id.id.hex}"
  *   resource_group_name       = azurerm_resource_group.this.name
  *   location                  = var.location
  *   account_tier              = "Standard"
  *   account_replication_type  = "LRS"
  *   enable_https_traffic_only = true
  * }
  * 
  * module "primary" {
  *  #source = "git::<repo-url>?ref=<tag>"
  *   source = "../../modules/azure-function"
  * 
  *   stage               = var.stage
  *   location            = var.location
  *   function_name       = "primary"
  *   os                  = "linux"
  *   resource_group_name = azurerm_resource_group.this.name
  *   storage_account = {
  *     name = "${var.stage}${random_id.id.hex}"
  *     key  = azurerm_storage_account.this.primary_access_key
  *   }
  *   app_service_plan_config = {
  *     kind = "FunctionApp",
  *     tier = "Dynamic",
  *     size = "Y1"
  *   }
  * }
  * 
  * module "secondary" {
  *  #source = "git::<repo-url>?ref=<tag>"
  *   source = "../../modules/azure-function"
  * 
  *   stage               = var.stage
  *   location            = var.location
  *   function_name       = "secondary"
  *   os                  = "windows"
  *   resource_group_name = azurerm_resource_group.this.name
  *   storage_account = {
  *     name = "${var.stage}${random_id.id.hex}"
  *     key  = azurerm_storage_account.this.primary_access_key
  *   }
  *   app_service_plan_config = {
  *     kind = "FunctionApp",
  *     tier = "Dynamic",
  *     size = "Y1"
  *   }
  * }
  * ```
  * # Related documentation
  * * [Terraform resource documentation](www.terraform.io/docs/providers/azurerm/r/app_service_plan.html)
  * * [Microsoft Azure documentation](docs.microsoft.com/en-us/azure/app-service/overview-hosting-plans)
*/
resource "random_id" "id" {
  byte_length = 4
}

# Limitations: https://docs.microsoft.com/en-us/azure/app-service/overview#limitations
resource "azurerm_app_service_plan" "this" {
  name                = var.function_name
  location            = var.location
  resource_group_name = var.resource_group_name
  kind                = var.app_service_plan_config.kind
  reserved            = "${ var.app_service_plan_config.kind == "Linux" ? true : false }"

  sku {
    tier = var.app_service_plan_config.tier
    size = var.app_service_plan_config.size
  }
}

resource "azurerm_application_insights" "this" {
  name                = "${var.function_name}${random_id.id.hex}"
  location            = var.location
  resource_group_name = var.resource_group_name
  application_type    = "other"
  retention_in_days   = 90
}

resource "azurerm_function_app" "this" {
  name                       = "${var.function_name}${random_id.id.hex}"
  location                   = var.location
  resource_group_name        = var.resource_group_name
  app_service_plan_id        = azurerm_app_service_plan.this.id
  storage_account_name       = var.storage_account.name
  storage_account_access_key = var.storage_account.key
  enable_builtin_logging     = var.builtin_logging
  os_type                    = "${ var.os == "linux" ? "linux" : "" }"   # Pending https://github.com/terraform-providers/terraform-provider-azurerm/issues/11859
  version                    = "~3"
  
  app_settings = {
    "APPINSIGHTS_INSTRUMENTATIONKEY" = "${azurerm_application_insights.this.instrumentation_key}"
  }
}