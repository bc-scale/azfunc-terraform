variable "function_name" {
  type    = string
  default = null
}

variable "stage" {
  type    = string
  default = null
}

variable "resource_group_name" {
  type    = string
  default = null
}

variable "location" {
  type    = string
  default = null
}

variable "storage_account" {
  type    = map
  default = {
    name = null,
    key = null
  }
}

#References for Az Service Plan sku differences
#https://docs.microsoft.com/en-us/azure/azure-resource-manager/management/azure-subscription-service-limits#app-service-limits
#https://blog.siliconvalve.com/2016/05/12/understanding-azure-app-service-plans-and-pricing/

variable "app_service_plan_config" {
  type    = map
  default = {
    kind = "FunctionApp",
    tier = "Dynamic",
    size = "Y1"
  }

  validation {
    condition     = contains(["FunctionApp", "Linux", "Windows", "elastic"], var.app_service_plan_config.kind)
    error_message = "Valid values for app_service_plan_config.kind are 'FunctionApp' (Consumption Plan), 'elastic' (Premium Consumption), 'Windows' or 'Linux' (Dedcated/Shared compute)."
  }
  validation {
    condition     = contains(["Dynamic", "Basic", "Standard", "PremiumV2", "PremiumV3", "Free", "Shared"], var.app_service_plan_config.tier)
    error_message = "Valid values for app_service_plan_config.tier are 'Dynamic' (Consumption Plan), 'Standard', 'Basic', 'PremiumV2', 'PremiumV3' (Dedcated compute) or 'Free', 'Shared' (Shared compute)."
  }
}

variable "os" {
  type    = string
  description = "OS of function app. Must be 'linux' or 'windows'"
  default = null

  validation {
    condition     = contains(["linux", "windows"], var.os)
    error_message = "Valid values for var: os are linux | windows."
  }
}

variable "builtin_logging" {
  type    = bool
  default = true
}