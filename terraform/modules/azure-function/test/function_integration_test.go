package test

import (
	"fmt"
	"strings"
	"testing"
	"time"

	http_helper "github.com/gruntwork-io/terratest/modules/http-helper"
	"github.com/gruntwork-io/terratest/modules/terraform"
	//"github.com/stretchr/testify/assert"
)

type FunctionAppResource struct{}

func TestLinuxFunctionAppDefault(t *testing.T) {
	// Construct the terraform options with default retryable errors to handle the most common
	// retryable errors in terraform testing.
	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		// Set the path to the Terraform code that will be tested.
		TerraformDir: "fixtures",
		// Use the var file
		VarFiles: []string{"default.tfvars"},
	})

	// Clean up resources with "terraform destroy" at the end of the test.
	defer terraform.Destroy(t, terraformOptions)

	// Run "terraform init" and "terraform apply". Fail the test if there are any errors.
	terraform.InitAndApply(t, terraformOptions)

	// Run `terraform output` to get the values of output variables and check they have the expected values.
	primaryUrl := terraform.Output(t, terraformOptions, "primary_url")

	// Make a HTTP request to the azFunction, make sure returns HTTP 200 with "Your Azure Function App is up and running"
	url := fmt.Sprintf("https://%s", primaryUrl)
	http_helper.HttpGetWithRetryWithCustomValidation(t, url, nil, 3, 5*time.Second, verifyFunctionDefaultPage)
}

func verifyFunctionDefaultPage(statusCode int, body string) bool {
	if statusCode != 200 {
		return false
	}
	return strings.Contains(body, "Azure Functions is an event-based serverless compute experience to accelerate your development.")
}
