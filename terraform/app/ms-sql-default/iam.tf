# Use existing azureAD group
data "azuread_group" "this" {
  display_name     = "dev"
  security_enabled = true
}

resource "azurerm_role_assignment" "this" {
  scope                = azurerm_resource_group.this.id
  role_definition_name = "Contributor"
  principal_id         = data.azuread_group.this.id
}