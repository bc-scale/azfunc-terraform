stage = "test"
location = "uksouth"
rg_name = "terraform-test"
function_name = "terraform-test"

app_service_plan_config = {
    kind        = "FunctionApp"
    reserved    = true
    teir        = "Basic"
    size        = "B1"
}