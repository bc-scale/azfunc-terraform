- Used this [azure app service python quickstart](https://docs.microsoft.com/en-us/azure/app-service/quickstart-python?tabs=bash&pivots=python-framework-flask) to deploy resources which can be imported into a new terraform config
- Used [this Terraform tutorial to import the existing app service and it's resource dependenies](https://learn.hashicorp.com/tutorials/terraform/state-import?in=terraform/state)

Azure resource imports use the Resource ID. Find this in the Azure Portal, resource Overview\JSON view or via the Azure CLI, e.g.:
```
$ az webapp list --query [].id -o tsv
/subscriptions/f7151251-422b-4507-a919-63b5c24319d3/resourceGroups/alex.j.chadwick_rg_Linux_uksouth/providers/Microsoft.Web/sites/python-docs-hello-world-28972
```

## Initial import
Working dir == `terraform/app/import-app-service`
```
> terraform init -backend-config="env/backend-dev"
> terraform import azurerm_app_service.import /subscriptions/f7151251-422b-4507-a919-63b5c24319d3/resourceGroups/alex.j.chadwick_rg_Linux_uksouth/providers/Microsoft.Web/sites/python-docs-hello-world-28972
```

View the resource config as imported into Terraform:
```
terraform show
```
View the required minimum config for the imported resource:
```
terraform plan
```
The plan output will show issues with the current config which need to be resolved:
- "Missing required argument" errors show the minimum required config
- "invalid or unknown key" errors for read-only properties which have been copied from `terraform show`. Delete these properties e.g. "id"
- Any resource properties which trigger a replacement of the existing Azure resource
- Any resource properties which cause unwanted updates to an existing value

OR copy the entire config into the tf file:
```
terraform show -no-color > main.tf
```
- Reference the [Terraform provider documentation for the resource](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/app_service#example-usage)

## Remove imported resources

Undo any imports by:
- Deleting the resource block from the tf file
- Removing the resource from the tfstate
```
#Find the tf state resources
> terraform state list
azurerm_app_service.import
azurerm_app_service_plan.import
azurerm_resource_group.import

#Delete the one you want to remove
> terraform state rm azurerm_app_service.import
```